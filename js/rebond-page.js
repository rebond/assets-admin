(function($) { 
    $.fn.tree = function(settings) {
        var options = { expand: {} };
        $.extend(options, settings);

        return this.each(function() {
            var $tree = $(this).addClass('tree');

            $(options.expand, $tree).each(function() {
                $('ul ul', $tree).hide();
                $('ul:first', $tree).show();
                $(this).addClass('open');

                $('li', $tree).removeClass('file folder');
                $('li:not(:has(li))', $tree).addClass('file');
                $('li:has(li)', $tree).addClass('folder');

                var id = R.url.queryMap('id');
                if (R.isset(id) && id > 0) {
                    enablePage($('#p_' + id + ' > a'));
                }

                var $selected = $('.selected', $tree);
                if ($selected.length) {
                    var $lis = $selected.parents('li');
                    $lis.each(function(i) {
                        if (i >= 1 && i != ($lis.length - 1)) {
                            $(this).addClass('open');
                            $(this).children('ul').toggle();
                        }
                    });
                }
            });

            // open folder
            $('.folder > a > .icon', $tree).click(function(e) {
                var $li = $(this).parent().parent();
                $li.children('ul').toggle();
                $li.toggleClass('open');
                e.stopPropagation();
                return false;
            });

            $('a', $tree).click(function() {
                enablePage($(this));
                return false;
            });

            $('li', $tree).click(function(e) {
                e.stopPropagation();
            });

            function enablePage(title) {
                $('a', $tree).removeClass('selected');
                title.addClass('selected');

                var pageId = parseInt(title.data('id'));

                $('#new-page').attr('href', '/page/edit/?pid=' + pageId);
                $('#edit-page').removeClass('disabled').attr('href','/page/edit/?id=' + pageId);
                $('#edit-gadget').removeClass('disabled').attr('href','/page/gadget/?id=' + pageId);
                if (pageId < 10) {
                    $('#delete-page').addClass('disabled');
                } else {
                    $('#delete-page').removeClass('disabled');
                }
                $('#del-page').val(pageId);
            }
        });
    };
})(jQuery);